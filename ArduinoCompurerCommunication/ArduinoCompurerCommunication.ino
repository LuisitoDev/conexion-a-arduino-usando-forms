String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
String commandString = "";

int ledPin = 19;
int vibratorMotorPin = 20;

int carRotation = 0;
int carSpeed = 0;

boolean isConnected = false;

void setup() {
  
  Serial.begin(9600);
  pinMode(A0,INPUT);
  pinMode(A1,INPUT);
  pinMode(ledPin,OUTPUT);
  pinMode(vibratorMotorPin,OUTPUT);
  
}

void loop() {
carRotation = analogRead(A0);
carSpeed = analogRead(A1);
Serial.print(map(carSpeed, 0, 1023, -100, 100));
Serial.print(",");
Serial.print(map(carRotation, 0, 1023, -100, 100));
delay(50); //TODO: REVISAR SI ESTE delay SIRVE TAMBIÉN PARA EL BEEP
 
if(stringComplete)
{
  stringComplete = false;
  getCommand();
  
  if(commandString.equals("STAR")) 
  {
  }
  else if(commandString.equals("STOP"))
  {
    turnLedOff(ledPin); 
  }
  else if(commandString.equals("LED"))
  {
    boolean LedState = getLedState();
    if(LedState == true)
    {
      turnLedOn(ledPin);
    }else
    {
      turnLedOff(ledPin);
    }   
  }
  

  inputString = "";
 }

}


boolean getLedState() //Revisamos si el comando es un #LEDON o es un #LEDOFF
{
  boolean state = false;
  if(inputString.substring(5,7).equals("ON")) 
  {
    state = true;
  }else
  {
    state = false;
  }
  return state;
}

void getCommand()
{
  if(inputString.length()>0)
  {
     commandString = inputString.substring(1,5);
  }
}

void turnLedOn(int pin)
{
  digitalWrite(pin,HIGH);
}

void turnLedOff(int pin)
{
  digitalWrite(pin,LOW);
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
