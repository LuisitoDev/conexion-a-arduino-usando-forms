﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading;


namespace ComputerToArduino
{
    public partial class Form1 : Form

    {
        Conexion_a_arduino.ControlesArduino controlesVolante;

        String[] ports;

        float v = 0; //v = vertical (carSpeed)
        float h = 0; //h = horizontal (carDirection)


        public Form1()
        {
            InitializeComponent();
            
            ports = SerialPort.GetPortNames();
            controlesVolante = new Conexion_a_arduino.ControlesArduino(ports[0]);
            TB_SERIAL_PORT.Text = ports[0];
        }

        private void LedCheckboxClicked(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                controlesVolante.turnLED(true);
            }
            else
            {
                controlesVolante.turnLED(false);
            }
        }
        
        
        
        private void BTN_SEND_BEEP_Click(object sender, EventArgs e)
        {
            controlesVolante.sendBEEP();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            controlesVolante.updateValues();
            h = controlesVolante.getValorHorizontal();
            v = controlesVolante.getValorVertical();
            TB_SPEED.Text = h.ToString();
            TB_DIRECTION.Text = v.ToString();
        }
        
        
    }
}
