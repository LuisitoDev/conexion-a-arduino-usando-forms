﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports; //Library to read our arduino data
using System.Collections; //to use Ienumerator in our code

namespace Conexion_a_arduino
{
    public class ControlesArduino
    {
        SerialPort seri;
        string selectedPort;
        float valorHorizontal = 0;
        float valorVertical = 0;

        private void connectToArduino()
        {           
            seri = new SerialPort(this.selectedPort, 9600);
            seri.Open();
        }

        public ControlesArduino(string selectedPort)
        {
            this.selectedPort = selectedPort;
            connectToArduino();
        }
       
        public void turnLED(bool turn)
        {
            if (turn == true)
            {
                seri.Write("#LEDON;");
            }
            else
            {
                seri.Write("#LEDOFF;");
            }
        }

        public void sendBEEP()
        {
            seri.Write("#BEEP;");
        }
        
        public float getValorHorizontal()
        {
            return valorHorizontal;

        }
        
        public float getValorVertical()
        {
            return valorVertical;
        }

        public void updateValues()
        {
            try
            {
                string cadenaArduino = seri.ReadLine();
                string[] values = new string[2];
                values = cadenaArduino.Split(',');

                valorHorizontal = (float.Parse(values[0]));
                valorVertical = (float.Parse(values[1]));
            }
            catch (Exception)
            {

            }
        }


    }
}


