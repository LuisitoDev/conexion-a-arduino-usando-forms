﻿namespace ComputerToArduino
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.TB_SERIAL_PORT = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.BTN_SEND_BEEP = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TB_SPEED = new System.Windows.Forms.TextBox();
            this.LAVEL_SPEED = new System.Windows.Forms.Label();
            this.TB_DIRECTION = new System.Windows.Forms.TextBox();
            this.LABLE_DIRECTION = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(14, 45);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(67, 24);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "LED";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.LedCheckboxClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Location = new System.Drawing.Point(218, 35);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox1.Size = new System.Drawing.Size(142, 105);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "LED Control";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.TB_SERIAL_PORT);
            this.groupBox2.Location = new System.Drawing.Point(14, 241);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox2.Size = new System.Drawing.Size(188, 90);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial Connection";
            // 
            // TB_SERIAL_PORT
            // 
            this.TB_SERIAL_PORT.Enabled = false;
            this.TB_SERIAL_PORT.Location = new System.Drawing.Point(8, 38);
            this.TB_SERIAL_PORT.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.TB_SERIAL_PORT.MaxLength = 10;
            this.TB_SERIAL_PORT.Name = "TB_SERIAL_PORT";
            this.TB_SERIAL_PORT.Size = new System.Drawing.Size(126, 26);
            this.TB_SERIAL_PORT.TabIndex = 19;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.BTN_SEND_BEEP);
            this.groupBox3.Location = new System.Drawing.Point(218, 175);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox3.Size = new System.Drawing.Size(158, 156);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Vibrator motor";
            // 
            // BTN_SEND_BEEP
            // 
            this.BTN_SEND_BEEP.Location = new System.Drawing.Point(26, 44);
            this.BTN_SEND_BEEP.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.BTN_SEND_BEEP.Name = "BTN_SEND_BEEP";
            this.BTN_SEND_BEEP.Size = new System.Drawing.Size(116, 86);
            this.BTN_SEND_BEEP.TabIndex = 0;
            this.BTN_SEND_BEEP.Text = "Send Beep";
            this.BTN_SEND_BEEP.UseVisualStyleBackColor = true;
            this.BTN_SEND_BEEP.Click += new System.EventHandler(this.BTN_SEND_BEEP_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TB_SPEED
            // 
            this.TB_SPEED.Enabled = false;
            this.TB_SPEED.Location = new System.Drawing.Point(10, 63);
            this.TB_SPEED.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.TB_SPEED.Name = "TB_SPEED";
            this.TB_SPEED.Size = new System.Drawing.Size(163, 26);
            this.TB_SPEED.TabIndex = 14;
            // 
            // LAVEL_SPEED
            // 
            this.LAVEL_SPEED.AutoSize = true;
            this.LAVEL_SPEED.Location = new System.Drawing.Point(6, 34);
            this.LAVEL_SPEED.Name = "LAVEL_SPEED";
            this.LAVEL_SPEED.Size = new System.Drawing.Size(56, 20);
            this.LAVEL_SPEED.TabIndex = 15;
            this.LAVEL_SPEED.Text = "Speed";
            // 
            // TB_DIRECTION
            // 
            this.TB_DIRECTION.Enabled = false;
            this.TB_DIRECTION.Location = new System.Drawing.Point(10, 117);
            this.TB_DIRECTION.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.TB_DIRECTION.Name = "TB_DIRECTION";
            this.TB_DIRECTION.Size = new System.Drawing.Size(163, 26);
            this.TB_DIRECTION.TabIndex = 16;
            // 
            // LABLE_DIRECTION
            // 
            this.LABLE_DIRECTION.AutoSize = true;
            this.LABLE_DIRECTION.Location = new System.Drawing.Point(6, 92);
            this.LABLE_DIRECTION.Name = "LABLE_DIRECTION";
            this.LABLE_DIRECTION.Size = new System.Drawing.Size(72, 20);
            this.LABLE_DIRECTION.TabIndex = 17;
            this.LABLE_DIRECTION.Text = "Direction";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LABLE_DIRECTION);
            this.groupBox4.Controls.Add(this.TB_DIRECTION);
            this.groupBox4.Controls.Add(this.LAVEL_SPEED);
            this.groupBox4.Controls.Add(this.TB_SPEED);
            this.groupBox4.Location = new System.Drawing.Point(12, 3);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.groupBox4.Size = new System.Drawing.Size(189, 200);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Data from serial port";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 345);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.Name = "Form1";
            this.Text = "Computer to Arduino";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button BTN_SEND_BEEP;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox TB_SPEED;
        private System.Windows.Forms.Label LAVEL_SPEED;
        private System.Windows.Forms.TextBox TB_DIRECTION;
        private System.Windows.Forms.Label LABLE_DIRECTION;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox TB_SERIAL_PORT;
    }
}

